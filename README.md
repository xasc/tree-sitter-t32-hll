# tree-sitter-t32-hll

Lauterbach TRACE32 script language C/C++ extension grammar for [tree-sitter](https://github.com/tree-sitter/tree-sitter) and [[tree-sitter-t32](https://codeberg.org/xasc/tree-sitter-t32.git)]. This grammar was adapted from [tree-sitter-c](https://github.com/tree-sitter/tree-sitter-c.git).

## References

- [General Commands Reference Guide V](https://www.lauterbach.com/pdf/general_ref_v.pdf)
- [PowerView User’s Guide](https://www.lauterbach.com/pdf/ide_user.pdf)
- [Training Script Language PRACTICE](https://www.lauterbach.com/pdf/training_practice.pdf)
